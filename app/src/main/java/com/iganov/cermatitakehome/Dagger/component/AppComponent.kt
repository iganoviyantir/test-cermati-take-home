package com.iganov.cermatitakehome.Dagger.component

import com.iganov.cermatitakehome.App.ApplicationController
import com.iganov.cermatitakehome.Connection.EndPointServices
import com.iganov.cermatitakehome.Dagger.modul.AppModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AppModule::class))
interface AppComponent {

    fun MemberServices(): EndPointServices
    fun inject(myApp : ApplicationController)

}