package com.iganov.cermatitakehome.Dagger.component

import com.iganov.cermatitakehome.Dagger.modul.MainModule
import com.iganov.cermatitakehome.Dagger.scope.ActivityScope
import com.iganov.cermatitakehome.View.MainActivity
import dagger.Component


@ActivityScope
@Component(modules = arrayOf(MainModule::class), dependencies = arrayOf(AppComponent::class))
interface MainComponent {
    fun inject (main : MainActivity)
}