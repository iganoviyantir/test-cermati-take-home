package com.iganov.cermatitakehome.Connection

import com.iganov.cermatitakehome.App.AppConstant
import com.iganov.cermatitakehome.App.base.BaseView
import retrofit2.adapter.rxjava2.HttpException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

object ErrorHandler {

    fun handlerErrorPresenter(mvpView: BaseView, throwable: Throwable) {
        val message = handleError(throwable)
        mvpView.onStopLoading()
        mvpView.onFailed(message!!)
    }

    fun handleError(throwable: Throwable?): String? {
        Logger.log(throwable)

        if (throwable == null) return AppConstant.NETWORK_ERROR

        if (throwable is HttpException) {
            val httpException = throwable as HttpException?
            if(httpException!!.code() == 403 || httpException!!.code() == 422 || httpException!!.code() == 400 ) {
                return httpException.code().toString()
            }
        }

        if(throwable is ConnectException)
        {
            return AppConstant.NETWORK_ERROR
        }

        if(throwable is UnknownHostException)
        {
            return AppConstant.NETWORK_ERROR
        }

        if(throwable is SocketTimeoutException)
        {
            return AppConstant.CONNECTION_TIMEOUT
        }

        return AppConstant.NETWORK_ERROR
    }
}