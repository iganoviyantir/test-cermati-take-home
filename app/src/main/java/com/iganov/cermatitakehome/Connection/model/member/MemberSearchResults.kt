package com.iganov.cermatitakehome.Connection.model.member

data class MemberSearchResults(
    val incomplete_results: Boolean,
    val items: List<MemberResponseItem>,
    val total_count: Int
)