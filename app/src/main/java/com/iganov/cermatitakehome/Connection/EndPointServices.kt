package com.iganov.cermatitakehome.Connection

import com.iganov.cermatitakehome.Connection.model.member.MemberResponseItem
import com.iganov.cermatitakehome.Connection.model.member.MemberResponseList
import com.iganov.cermatitakehome.Connection.model.member.MemberSearchResults
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface EndPointServices {

    @GET("/users")
    fun getUserList(@Query("page") page : String, @Query("per_page") per_page : String,@Query("since") since : String) : Observable<MemberResponseList>

    @GET("/users/{username}")
    fun getDetailUser(@Path("username") username : String) : Observable<MemberResponseItem>

    @GET("/search/users")
    fun searchUserByUsername(@Query("q") username : String,@Query("page") page : String,@Query("per_page") per_page : String) : Observable<MemberSearchResults>
}