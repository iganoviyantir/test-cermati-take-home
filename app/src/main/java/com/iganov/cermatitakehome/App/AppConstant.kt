package com.iganov.cermatitakehome.App

object AppConstant {

    const val APP_NAME = "Cermati Test"
    const val BASE_URL = "https://api.github.com"
    const val NETWORK_ERROR = "Tidak ada koneksi internet"
    const val CONNECTION_TIMEOUT = "Connection Time Out. Try Again"

    const val header_key = "Accept"
    const val header_value = "application/vnd.github.v3+json"

    const val CODE_403= "403"
    const val CODE_404= "404"
    const val CODE_406= "406"
    const val CODE_400= "400"
    const val CODE_402= "402"
    const val CODE_500= "500"
    const val CODE_422= "422"
}