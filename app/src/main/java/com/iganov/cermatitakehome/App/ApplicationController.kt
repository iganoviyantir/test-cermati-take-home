package com.iganov.cermatitakehome.App

import android.app.Application
import com.iganov.cermatitakehome.Dagger.component.AppComponent
import com.iganov.cermatitakehome.Dagger.component.DaggerAppComponent
import com.iganov.cermatitakehome.Dagger.modul.AppModule
import com.iganov.cermatitakehome.R
import org.acra.ACRA
import org.acra.ReportingInteractionMode
import org.acra.annotation.ReportsCrashes

@ReportsCrashes(
    mailTo = "sendme.crashreport@gmail.com",
    mode = ReportingInteractionMode.TOAST,
    resToastText = R.string.crash_toast_text
)// my email here

class ApplicationController : Application(){

    val TAG = ApplicationController::class.java
        .getSimpleName()

    var appComponent : AppComponent?=null
    private var mInstance: ApplicationController? = null

    override fun onCreate() {
        super.onCreate()
        ACRA.init(this)
        mInstance = this

        iniDagger()
    }

    @Synchronized
    fun getInstance(): ApplicationController? {
        return mInstance
    }

    private fun iniDagger() {
        appComponent = DaggerAppComponent.builder().appModule(AppModule(this)).build()
    }
}