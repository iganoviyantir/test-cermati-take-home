package com.iganov.cermatitakehome.App.base

import android.os.Bundle
import com.iganov.cermatitakehome.App.ApplicationController
import com.iganov.cermatitakehome.Dagger.component.AppComponent
import com.iganov.cermatitakehome.Dagger.component.DaggerMainComponent
import com.iganov.cermatitakehome.Dagger.component.MainComponent
import com.trello.rxlifecycle.components.support.RxAppCompatActivity

abstract class BaseActivity : RxAppCompatActivity()  {

    protected abstract val layoutRes: Int

    lateinit var maincomponent : MainComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutRes)

        initDagger()
    }

    fun initDagger(){
        maincomponent = DaggerMainComponent.builder().appComponent(getAppComponent()).build()
    }

    fun getAppComponent() : AppComponent? = (applicationContext as ApplicationController).appComponent
}