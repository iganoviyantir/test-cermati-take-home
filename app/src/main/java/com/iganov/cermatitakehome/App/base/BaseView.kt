package com.iganov.cermatitakehome.App.base

interface BaseView {
    fun onLoading()
    fun onStopLoading()
    fun onFailed(message: String)
    fun onNotFound(message : String)
}