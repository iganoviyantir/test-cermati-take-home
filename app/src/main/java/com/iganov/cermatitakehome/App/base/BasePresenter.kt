package com.iganov.cermatitakehome.App.base


interface BasePresenter<in T: BaseView> {
    fun attachView(view: T)
    fun detachView()
    fun <T : BaseView> getView(): T
}