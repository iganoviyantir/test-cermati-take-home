package com.iganov.cermatitakehome.Utility

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class LinearLayoutManagerWrapper : LinearLayoutManager {

    constructor(context: Context) : super(context)
    var rv : RecyclerView? = null

    constructor(context: Context, orientation: Int, reverseLayout: Boolean, rv : RecyclerView? = null) : super(
        context,
        orientation,
        reverseLayout
    )
    {
        this.rv = rv
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(
        context,
        attrs,
        defStyleAttr,
        defStyleRes
    )

    override fun supportsPredictiveItemAnimations(): Boolean {
        return false
    }

    override fun scrollHorizontallyBy(
        dx: Int,
        recycler: RecyclerView.Recycler?,
        state: RecyclerView.State?
    ): Int {
        if(rv == null)
        {
            return super.scrollHorizontallyBy(dx, recycler, state)
        }
        var nScroll = 0

        if(rv?.scrollState != RecyclerView.SCROLL_STATE_SETTLING)
        {
            nScroll = super.scrollHorizontallyBy(dx, recycler, state)
        }

        return nScroll
    }
}