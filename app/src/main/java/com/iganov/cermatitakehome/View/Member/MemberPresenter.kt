package com.iganov.cermatitakehome.View.Member

import com.iganov.cermatitakehome.App.base.BaseView
import com.iganov.cermatitakehome.Connection.EndPointServices
import com.iganov.cermatitakehome.Connection.ErrorHandler
import com.iganov.cermatitakehome.Connection.model.member.MemberResponseItem
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MemberPresenter  @Inject constructor(private val endPointServices: EndPointServices) : MemberContract.memberPresenter {

    private var view : MemberContract.memberView? = null
    private var disposables = CompositeDisposable()

    override fun getMemberList(page: String, per_page: String, since : String) {
        view?.onLoading()
        disposables.add(endPointServices.getUserList(page, per_page,since)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe (
                { intro ->
                    view?.onStopLoading()
                    view?.setMemberList(intro)
                },
                {t: Throwable? ->
                    ErrorHandler.handlerErrorPresenter(getView(),t!!)
                })
        )
    }

    override fun getMemberDetail(username: String) {
        view?.onLoading()
        disposables.add(endPointServices.getDetailUser(username)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe (
                { intro ->
                    view?.onStopLoading()
                    view?.setMemberDetail(intro)
                },
                {t: Throwable? ->
                    ErrorHandler.handlerErrorPresenter(getView(),t!!)
                })
        )
    }

    override fun searchUserByUsername(q: String,page : String, per_page : String) {
        view?.onLoading()
        disposables.add(endPointServices.searchUserByUsername(q, page, per_page)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe (
                { intro ->
                    view?.onStopLoading()
                    view?.setMemberList(intro.items as ArrayList<MemberResponseItem>)
                },
                {t: Throwable? ->
                    ErrorHandler.handlerErrorPresenter(getView(),t!!)
                })
        )
    }

    override fun attachView(view: MemberContract.memberView) {
        this.view = view
    }

    override fun detachView() {
        view = null
        disposables.clear()
    }

    override fun <T : BaseView> getView(): T {
        return view as T
    }
}