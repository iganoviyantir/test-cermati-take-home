package com.iganov.cermatitakehome.View

import android.app.AlertDialog
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.facebook.shimmer.ShimmerFrameLayout
import com.iganov.cermatitakehome.Adapter.RecyclerView.ListMemberAdapter
import com.iganov.cermatitakehome.App.AppConstant
import com.iganov.cermatitakehome.App.base.BaseActivity
import com.iganov.cermatitakehome.Connection.model.member.MemberResponseItem
import com.iganov.cermatitakehome.Connection.model.member.MemberResponseList
import com.iganov.cermatitakehome.R
import com.iganov.cermatitakehome.Utility.LinearLayoutManagerWrapper
import com.iganov.cermatitakehome.View.Member.MemberContract
import com.iganov.cermatitakehome.View.Member.MemberPresenter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.loading_screen.*
import org.jetbrains.anko.toast
import javax.inject.Inject

class MainActivity : BaseActivity(), MemberContract.memberView, ListMemberAdapter.ListMemberAdapterCallback {

    override val layoutRes: Int
        get() = R.layout.activity_main

    private var page = 1
    private var per_page = 20
    private var isSearch = false
    private var query  = ""
    lateinit var adapter : ListMemberAdapter
    var listMember = ArrayList<MemberResponseItem>()
    var status : Boolean = false
    lateinit var shimmerView : ShimmerFrameLayout
    var isDialogShow = false

    @Inject
    lateinit var presenter : MemberPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        maincomponent.inject(this)
        presenter.attachView(this)

        initView()
    }

    fun initView(){

        shimmerView = findViewById(R.id.shimmer_view_container)
        val layoutManager = LinearLayoutManagerWrapper(this, LinearLayoutManager.VERTICAL, false)
        rvListUsers.layoutManager = layoutManager
        rvListUsers.isNestedScrollingEnabled = true
        rvListUsers.addItemDecoration(DividerItemDecoration(rvListUsers.context, DividerItemDecoration.VERTICAL))

        adapter = ListMemberAdapter(this@MainActivity, listMember)
        rvListUsers.adapter = adapter

        rvListUsers.addOnScrollListener(object : RecyclerView.OnScrollListener(){
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
                val countItem = layoutManager.itemCount
                val lastVisiblePosition = linearLayoutManager.findLastCompletelyVisibleItemPosition()
                val isLastPosition = countItem.minus(1) == lastVisiblePosition
                status = true
                if(status && isLastPosition)
                {
                    page += 1
                    Handler().postDelayed({
                        presenter.searchUserByUsername(query, page.toString(), per_page.toString())
                    }, 500)
                }
            }
        })

        searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(newText: String?): Boolean {
                if(newText=="")
                {
                    rvListUsers.adapter = null
                    isSearch = false
                    listMember.clear()
                    page = 1
                }else {
                    rvListUsers.adapter = adapter
                    isSearch = true
                    query = newText!!
                    listMember.clear()
                    Handler().postDelayed({
                        presenter.searchUserByUsername(query, page.toString(), per_page.toString())
                    }, 700)
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if(newText=="")
                {
                    rvListUsers.adapter = null
                    isSearch = false
                    listMember.clear()
                    page = 1
                }else {
                    rvListUsers.adapter = adapter
                    isSearch = true
                    query = newText!!
                    listMember.clear()
                    Handler().postDelayed({
                        presenter.searchUserByUsername(query, page.toString(), per_page.toString())
                    }, 700)

                }
                return false
            }

        })
    }


    override fun setMemberList(r: ArrayList<MemberResponseItem>) {
        if(!status) {
            listMember.clear()
        }
        for(x in 0..r.size.minus(1))
        {
            if(!listMember.contains(r[x])) {
                listMember.add(r[x])
            }
            adapter.notifyDataSetChanged()
        }
    }

    override fun setMemberDetail(r: MemberResponseItem) {
    }

    override fun onLoading() {
        if(listMember.size==0) {
            shimmerView.visibility = View.VISIBLE
        }
    }

    override fun onStopLoading() {
        if(listMember.size==0) {
            shimmerView.visibility = View.GONE
        }
    }

    override fun onFailed(message: String) {
        if(message == AppConstant.CODE_403)
        {
            if(!isDialogShow) {
                showAlert(getString(R.string.sorry), getString(R.string.warningRateLimit))
            }
        }else if(message == AppConstant.CODE_422)
        {
            rvListUsers.adapter = null
        }else if(message == AppConstant.CODE_400)
        {
            toast(getString(R.string.badrequest))
        }else if(message == AppConstant.CODE_500)
        {
            toast(getString(R.string.errorfromServer))
        }else if(message == AppConstant.CODE_404)
        {
            toast(getString(R.string.warningNotFound))
        }else
        {
            toast(message)
        }
    }

    fun showAlert(title : String, message: String)
    {
        isDialogShow = true
        val alertDialogBuilder =
            androidx.appcompat.app.AlertDialog.Builder(
                this
            )
        alertDialogBuilder.setTitle(title)
        alertDialogBuilder
            .setMessage(message)
            .setCancelable(false)
            .setPositiveButton("Ok") { dialog, id ->
                dialog.dismiss()
                isDialogShow = false
            }
        val alertDialog = alertDialogBuilder.create()
        alertDialog.show()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    override fun informDataIsEmpty() {
        presenter.searchUserByUsername(query, page.toString(), per_page.toString())
    }

    override fun onNotFound(message: String) {
        if(!isDialogShow) {
            showAlert(getString(R.string.sorry), getString(R.string.warningNotFound))
        }
    }
}
