package com.iganov.cermatitakehome.View.Member

import com.iganov.cermatitakehome.App.base.BasePresenter
import com.iganov.cermatitakehome.App.base.BaseView
import com.iganov.cermatitakehome.Connection.model.member.MemberResponseItem
import com.iganov.cermatitakehome.Connection.model.member.MemberResponseList

class MemberContract {

    interface memberView : BaseView{
        fun setMemberList(r : ArrayList<MemberResponseItem>)
        fun setMemberDetail(r : MemberResponseItem)
    }

    interface memberPresenter : BasePresenter<memberView>{
        fun getMemberList(page : String, per_page : String, since : String)
        fun getMemberDetail(username : String)
        fun searchUserByUsername(q: String,page : String, per_page : String)
    }
}