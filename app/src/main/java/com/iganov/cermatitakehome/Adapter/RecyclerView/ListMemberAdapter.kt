package com.iganov.cermatitakehome.Adapter.RecyclerView

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.iganov.cermatitakehome.Connection.model.member.MemberResponseItem
import com.iganov.cermatitakehome.R
import kotlinx.android.synthetic.main.row_list_users.view.*
import java.lang.reflect.Member
import java.util.*
import kotlin.collections.ArrayList

class ListMemberAdapter (private val c : Context, private val listmember : ArrayList<MemberResponseItem>) :
    RecyclerView.Adapter<ListMemberAdapter.MyViewHolder>(), Filterable {

    private var TAG = ListMemberAdapter::class.simpleName
    var listMemberFiltered = ArrayList<MemberResponseItem>()
    lateinit var adapterCallback : ListMemberAdapterCallback

    init {
        listMemberFiltered = listmember
        try{
            adapterCallback = c as ListMemberAdapterCallback
        }catch (e : Exception){
            Log.d(TAG, c.getString(R.string.warningAdapterCallback))
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_list_users, parent, false)

        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listmember.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.tvUserName.text = listmember[position].login
        Glide.with(c).load(listmember[position].avatar_url).into(holder.itemView.ivUserImage)
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun getFilter(): Filter {
        return object : Filter(){
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    listMemberFiltered = listmember
                } else {
                    val resultList = ArrayList<MemberResponseItem>()
                    for (row in listmember) {
                        if (row.login.toLowerCase(Locale.ROOT).contains(charSearch.toLowerCase(Locale.ROOT))) {
                            resultList.add(row)
                        }
                    }
                    listMemberFiltered = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = listMemberFiltered
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                listMemberFiltered = results?.values as ArrayList<MemberResponseItem>
                notifyDataSetChanged()
                if(listMemberFiltered.size==0)
                {
                    adapterCallback.informDataIsEmpty()
                }
            }
        }
    }

    interface ListMemberAdapterCallback{
        fun informDataIsEmpty()
    }
}